﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.CvEnum;
using Emgu.CV.Util;
using System.Drawing;
using System.Windows.Forms;

namespace DECPROC
{
    //晚上图片检测，边缘检测
    /// <summary>
    ///  应用场景：晚上没补光情况，主要实现：边缘检测
    /// </summary>
    public class DetectProcesser_2 : DetectProcesser
    {
        public override void ProcessRequest(DetectRequest Request)
        {

            //base.ProcessRequest(Request);

            //if (Request.v0 < 85) //pic4 100
            //{
            //    CvInvoke.EqualizeHist(Request.Gray, Request.GrayToDst);

            //    //MessageBox.Show(Request.v0.ToString());
            //    int edgeThresh = 65; //67
            //    CvInvoke.Canny(Request.GrayToDst, Request.GrayToDst, edgeThresh, edgeThresh * 3); //方法3 方法4

            //    Mat elemenCanny = CvInvoke.GetStructuringElement(ElementShape.Rectangle, new System.Drawing.Size(2, 2), new Point(0, 0)); //15,15早上图片
            //    CvInvoke.MorphologyEx(Request.GrayToDst, Request.GrayToDst, MorphOp.Close, elemenCanny, new Point(0, 0), 1, BorderType.Default, new MCvScalar());  //方法1  方法3 方法4

            //    //Request.GrayToDst = Request.GrayToDst.ToImage<Gray, byte>().Erode(1).Mat; //pic2 水尺未干的时候不能腐蚀
            //    //if (Request.v0 > 55 && null !=NextProcesser)
            //    //{
            //    //    NextProcesser.ProcessRequest(Request);
            //    //    return;
            //    //}

            //}

            if (Request.BSaveMatchImages || (Request.v0 < 25 && Request.sdv < 10)) //pic4 100
            {
                CvInvoke.EqualizeHist(Request.Gray, Request.GrayToDst);

                //MessageBox.Show(Request.v0.ToString());
                int edgeThresh = 75; //67
                CvInvoke.Canny(Request.GrayToDst, Request.GrayToDst, edgeThresh, edgeThresh * 3); //方法3 方法4

                Mat elemenCanny = CvInvoke.GetStructuringElement(ElementShape.Rectangle, new System.Drawing.Size(3, 3), new Point(0, 0)); //15,15早上图片
                CvInvoke.MorphologyEx(Request.GrayToDst, Request.GrayToDst, MorphOp.Close, elemenCanny, new Point(0, 0), 1, BorderType.Default, new MCvScalar());  //方法1  方法3 方法4

                //Request.GrayToDst = Request.GrayToDst.ToImage<Gray, byte>().Erode(1).Mat; //pic2 水尺未干的时候不能腐蚀
                //if (Request.v0 > 55 && null !=NextProcesser)
                //{
                //    NextProcesser.ProcessRequest(Request);
                //    return;
                //}
                FindContoursENextProc(Request);
            }
            else
            {
                if (null != NextProcesser)
                    NextProcesser.ProcessRequest(Request);
            }
        
                   //    //均衡化 增加对比度
            //    CvInvoke.Equ}lizeHist(Request.Gray, Request.GrayToDst);


            //    //CvInvoke.Threshold(Request.Gray, Request.GrayToDst, 35, 255, ThresholdType.ToZero);

            //    int edgeThresh = 57; //67
            //    CvInvoke.Canny(Request.GrayToDst, Request.GrayToDst, edgeThresh, edgeThresh * 3); //方法3 方法4

            //    //膨胀
            //    //Request.GrayToDst = Request.GrayToDst.ToImage<Gray, byte>().Dilate(1).Mat; //方法4

            //    //腐蚀
            //    Request.GrayToDst = Request.GrayToDst.ToImage<Gray, byte>().Erode(1).Mat; //方法4

            //    Mat elemenCanny = CvInvoke.GetStructuringElement(ElementShape.Rectangle, new System.Drawing.Size(5, 5), new Point(0, 0)); //15,15早上图片
            //    CvInvoke.MorphologyEx(Request.GrayToDst, Request.GrayToDst, MorphOp.Close, elemenCanny, new Point(0, 0), 1, BorderType.Default, new MCvScalar());  //方法1  方法3 方法4

                

            //    //画线
            //    int w = Request.ROIRectangle.Width, h = Request.ROIRectangle.Height;
            //    CvInvoke.Line(Request.GrayToDst, new Point(w / 3, h), new Point(w * 5 / 6, h), new MCvScalar(0, 0, 0), 3);
            //}

            
        }

        public override string ToString()
        {
            return "Proc_2_Night";//|EqualizeHist|Erode|Canny|Dilate
        }
    }
}
