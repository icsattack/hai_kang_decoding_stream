﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.CvEnum;
using Emgu.CV.Util;
using System.Drawing;


namespace DECPROC
{
    //补充晚上图片
    /// <summary>
    /// 应用场景：晚上图片检测补充，主要实现：全局阈值化 大津算法Otsu，阈值取值平均亮度
    /// </summary>
    public class DetectProcesser_3 : DetectProcesser
    {
        public override void ProcessRequest(DetectRequest Request)
        {
            //base.ProcessRequest(Request);

            if (!Request.BSaveMatchImages && Request.v0 > 110 && null != NextProcesser)
            {
                NextProcesser.ProcessRequest(Request);
                return;
            }

            if (Request.BSaveMatchImages || (Request.v0 < 66 && Request.sdv < 60))
            {
                //均衡化 增加对比度
                CvInvoke.EqualizeHist(Request.Gray, Request.GrayToDst);

                //CvInvoke.Threshold(Request.GrayToDst, Request.GrayToDst, Request.avg, 255, ThresholdType.Binary); //120

                CvInvoke.Threshold(Request.GrayToDst, Request.GrayToDst, 0, 255, ThresholdType.Otsu); //120

                int iterations = 1;
                Size mSize;
                //if (Request.sdv > 23)
                //{
                //if (Request.sdv < 12)
                    //Request.GrayToDst = Request.GrayToDst.ToImage<Gray, byte>().Erode(1).Mat;
                mSize = (Request.sdv < 20)?new System.Drawing.Size(9, 9) : new System.Drawing.Size(5, 5);
                iterations = (Request.sdv < 20) ? 2 : 1;
                //}
                //else
                //{
                //    mSize = new System.Drawing.Size(12, 12);
                //    Request.GrayToDst = Request.GrayToDst.ToImage<Gray, byte>().Dilate(7).Mat;
                //    iterations = 3;
                //}

                Mat elemenCanny = CvInvoke.GetStructuringElement(ElementShape.Rectangle, mSize, new Point(0, 0)); //15,15早上图片
                CvInvoke.MorphologyEx(Request.GrayToDst, Request.GrayToDst, MorphOp.Close, elemenCanny, new Point(0, 0), iterations, BorderType.Default, new MCvScalar());  //方法1  方法3 方法4

                //Request.GrayToDst = Request.GrayToDst.ToImage<Gray, byte>().Dilate(5).Mat;

                //Mat elemenCanny1 = CvInvoke.GetStructuringElement(ElementShape.Rectangle, new System.Drawing.Size(1, 1), new Point(0, 0));
                //CvInvoke.MorphologyEx(Request.GrayToDst, Request.GrayToDst, MorphOp.Open, elemenCanny1, new Point(0, 0), 1, BorderType.Default, new MCvScalar()); //方法3

                //画线
                //int w = Request.ROIRectangle.Width, h = Request.ROIRectangle.Height;
                //CvInvoke.Line(Request.GrayToDst, new Point(w / 3, h), new Point(w * 2 / 3, h), new MCvScalar(0, 0, 0), 3);

                //划线：时和有上角连接起来，形成连通域。
                CvInvoke.Line(Request.GrayToDst, new Point(0, 0), Request.WaterRulerTopPoint, new MCvScalar(255, 255, 255), 2);

                FindContoursENextProc(Request);
            }
            else
            {
                if (null != NextProcesser)
                    NextProcesser.ProcessRequest(Request);
            }
        }

        public override string ToString()
        {
            return "Proc_3_Otsu"; //|EqualizeHist|Threshold|Erode|Dilate
        }
    }
}
