﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.CvEnum;
using Emgu.CV.Util;
using System.Drawing;
using System.Windows.Forms;

namespace DECPROC
{
    /// <summary>
    /// 应用场景：早上或光照不均匀图片，主要实现：局部自适应阈值化
    /// </summary>
    public class DetectProcesser_0 : DetectProcesser
    {
        //主要用于判断早上图片
        public override void ProcessRequest(DetectRequest Request)
        {
            //base.ProcessRequest(Request);

            ////太亮、太暗不检测
            //if (Request.v0 > 120 || Request.v0 < 55)
            //{
            //    if (null != NextProcesser)
            //        NextProcesser.ProcessRequest(Request);
            //    return;
            //}

            //太亮、太暗不检测 65 90 120 ((Request.v0 > 73 && Request.v0 < 100) || (Request.v0 > 120 && Request.v0 < 150)) (_v0 > 97 && Request.v0 < 100) || 
            //(Request.v0 > 125 && Request.v0 < 135 && Request.sdv > 57)||
            if (Request.BSaveMatchImages || ( //(Request.v0 > 130 && Request.v0 < 131 &&  Request.sdv > 57 && Request.sdv < 59) || 
                (Request.v0 > 66 && Request.v0 < 91 && Request.sdv > 36 && Request.sdv < 45) ||
                (Request.v0 > 115 && Request.v0 < 124 && Request.sdv > 60) ||
                (Request.v0 > 116 && Request.v0 < 123 && Request.sdv < 40) ||
                (Request.v0 > 110 && Request.v0 < 120 && Request.sdv > 48 && Request.sdv < 55))
                )// && Request.v0 < 137
            {
                //腐蚀
                //Request.GrayToDst = Request.Gray.Erode(2).Mat;

                //double mParam = Request.v0 < 130 ? -35 : -33; //早上图片亮度判断 p16 23

                double mParam = 9;
                mParam = Request.v0 > 110 && Request.sdv > 55 && Request.sdv < 70 ? 33 : mParam;
                //mParam = Request.v0 < 125 ? -3 : mParam;
                //mParam = Request.v0 > 129 ? -9 : mParam;
                CvInvoke.AdaptiveThreshold(Request.Gray, Request.GrayToDst, 255, AdaptiveThresholdType.GaussianC, ThresholdType.Binary, (int)mParam, -mParam); //5, -5 33

                //MessageBox.Show(Request.Gray.GetAverage().MCvScalar.V0.ToString());

                Mat elemenCanny = CvInvoke.GetStructuringElement(ElementShape.Rectangle, new System.Drawing.Size(7, 7), new Point(0, 0)); //12, -12
                CvInvoke.MorphologyEx(Request.GrayToDst, Request.GrayToDst, MorphOp.Close, elemenCanny, new Point(0, 0), 1, BorderType.Default, new MCvScalar());

                //Request.GrayToDst = Request.GrayToDst.ToImage<Gray, byte>().Erode(1).Mat;

                //画线
                //int w = Request.ROIRectangle.Width, h = Request.ROIRectangle.Height;
                //CvInvoke.Line(Request.GrayToDst, new Point(w / 3, h), new Point(w * 5 / 6, h), new MCvScalar(0, 0, 0), 3);

                FindContoursENextProc(Request);
            }
            else
            {
                if (null != NextProcesser)
                    NextProcesser.ProcessRequest(Request);
            }
        }

        public override string ToString()
        {
            return "Proc_0_Morning";//|Erode|AdaptiveThreshold|Close
        }
    }
}
