﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using System.Drawing;
using System.Windows.Forms;

namespace DECPROC
{
    public abstract class DetectProcesser
    {
        protected double _v0;
        protected double _avg;
        protected double _sdv;

        public DetectProcesser NextProcesser { get; set; }

        public abstract void ProcessRequest(DetectRequest Request);

        //{
            //if (null != Request.SrcImg)
            //{
            //    //设置ROI
            //    Request.SrcImg.ROI = Request.ROIRectangle;

            //    //HSV颜色分离
            //    var tempImg = new Mat();
            //    CvInvoke.CvtColor(Request.SrcImg, tempImg, ColorConversion.Bgr2Hsv);

            //    Request.Gray = Request.SrcImg.Mat.ToImage<Gray, byte>();

            //    _v0 = Request.Gray.GetAverage().MCvScalar.V0;

            //    MCvScalar sdv = new MCvScalar();
            //    Gray avg = new Gray();
            //    Request.Gray.AvgSdv(out avg, out sdv);
            //    _avg = avg.MCvScalar.V0;
            //    _sdv = sdv.V0;
            //    //MessageBox.Show("_v0:" + _v0 + "\n avg:" + avg.MCvScalar.V0 + "\n sdv:" + sdv.V0);

            //    //var imgs = tempImg.Split();
            //    //Request.Gray = imgs[2].ToImage<Gray, byte>();

            //    //MessageBox.Show(Request.Gray.GetAverage().MCvScalar.V0.ToString());

            //    //CvInvoke.Imshow("h", imgs[0]);
            //    //CvInvoke.Imshow("s", imgs[1]);
            //    //CvInvoke.Imshow("v", imgs[2]);

            //    Request.GrayToDst = new Mat(Request.Gray.Size, DepthType.Cv8U, 1);
            //    Request.GrayToDstDisplay = new Mat(Request.Gray.Size, DepthType.Cv8U, 1);
            //    //Request.Reslut = new Mat(Request.GrayToDst.Size, Emgu.CV.CvEnum.DepthType.Cv8U, 3);
            //}

        //}


        //查找区域
        protected void FindContoursENextProc(DetectRequest Request)
        {
            Request.GrayToDst.CopyTo(Request.GrayToDstDisplay);

            VectorOfVectorOfPoint vvp = new VectorOfVectorOfPoint();
            CvInvoke.FindContours(Request.GrayToDst, vvp, null, RetrType.External, ChainApproxMethod.ChainApproxSimple);

            for (int i = 0; i < vvp.Size; i++)
            {
                using (VectorOfPoint contour = vvp[i])
                {
                    Rectangle rect = CvInvoke.BoundingRectangle(contour);
                    int tmparea = rect.Height * rect.Width;
                    //int tmpareaC =  Request.ConnectPoint.X * Request.ConnectPoint.Y;

                    //查找起点为ROI的左上点
                    if (tmparea > (Request.ROIRectangle.Width * 2 / 3) * (Request.WaterRulerTopPoint.Y + 5) && rect.X == 1 && rect.Y == 1 && rect.Width > Request.SrcImg.Width / 2)// && rect.Height / rect.Width > 1.5)
                    {
                        if (rect.Height == (Request.SrcImg.Height - 2)) //&& rect.Width == (Request.SrcImg.Width - 2)
                            continue;

                        CvInvoke.Rectangle(Request.SrcImg, new Rectangle(rect.X, rect.Y, rect.Width, rect.Height), new MCvScalar(0, 255, 0), 2);
                        Request.IsProc = true;
                        Request.WaterRulerBottom = rect.Bottom;
                    }
                }
            }


            Request.Reslut = new Mat(Request.GrayToDst.Size, Emgu.CV.CvEnum.DepthType.Cv8U, 3);
            CvInvoke.DrawContours(Request.Reslut, vvp, -1, new MCvScalar(0, 255, 0));

            if (Request.IsProc)
            {
                PutText(Request);
            }

            if (!Request.IsProc && null != this.NextProcesser)
            {
                NextProcesser.ProcessRequest(Request);
            }
        }

        //图片中插入处理的方法名称
        private void PutText(DetectRequest Request)
        {
            if (Request.IsProc)
            {
                Request.ProcName = this.ToString();
                //取消ROI
                Request.SrcImg.ROI = Rectangle.Empty;

                List<string> strs = this.ToString().Split('|').ToList<string>();
                //strs.AddRange(new string[] {
                //        //"_v0:" + Request.v0,
                //        "avg:" + Math.Round(Request.avg,2),
                //        "sdv:" + Math.Round(Request.sdv,2)
                //});
                //List<string> strs = new List<string> { "water level:" + Convert.ToString(Request.WaterRulerBottom) };

                CvInvoke.PutText(Request.SrcImg, strs[0], new Point(1, 75), FontFace.HersheyTriplex, 1, new MCvScalar(255, 0, 255));
                for (int i = 1; i < strs.Count; i++)
                {
                    CvInvoke.PutText(Request.SrcImg, strs[i], new Point(1, 75 + 35 + 35 * (i - 1)), FontFace.HersheyTriplex, 1, new MCvScalar(0, 255, 0));
                }
            }
        }
    }
}
