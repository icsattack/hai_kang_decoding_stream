﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DECPROC
{
    public class RulerParam
    {
        public int Para1 { get; set; }
        public int Para2 { get; set; }
        public int Para3 { get; set; }
        public int Para4 { get; set; }
        public int RulerLen { get; set; }

    }
}
