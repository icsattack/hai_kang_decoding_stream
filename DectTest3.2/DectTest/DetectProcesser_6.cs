﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.CvEnum;
using Emgu.CV.Util;
using System.Drawing;


namespace DECPROC
{
    //补充晚上图片
    /// <summary>
    /// 应用场景：晚上图片检测补充，主要实现：全局阈值化 大津算法Otsu，阈值取值平均亮度
    /// </summary>
    public class DetectProcesser_6 : DetectProcesser
    {
        public override void ProcessRequest(DetectRequest Request)
        {
            //base.ProcessRequest(Request);

            if (!Request.BSaveMatchImages && Request.v0 > 110 && null != NextProcesser)
            {
                NextProcesser.ProcessRequest(Request);
                return;
            }

            if (Request.BSaveMatchImages || (Request.v0 < 66 && Request.sdv < 60))
            {
                //均衡化 增加对比度
                CvInvoke.EqualizeHist(Request.Gray, Request.GrayToDst);

                CvInvoke.Threshold(Request.GrayToDst, Request.GrayToDst, 0, 255, ThresholdType.Otsu); //120

                int iterations = 1;
                Size mSize;
                mSize = (Request.sdv < 20)?new System.Drawing.Size(9, 9) : new System.Drawing.Size(5, 5);
                iterations = (Request.sdv < 20) ? 2 : 1;

                Request.GrayToDst = Request.GrayToDst.ToImage<Gray, byte>().Erode(1).Mat;

                Mat elemenCanny = CvInvoke.GetStructuringElement(ElementShape.Rectangle, mSize, new Point(0, 0)); //15,15早上图片
                CvInvoke.MorphologyEx(Request.GrayToDst, Request.GrayToDst, MorphOp.Close, elemenCanny, new Point(0, 0), iterations, BorderType.Default, new MCvScalar());  //方法1  方法3 方法4

                //划线：时和有上角连接起来，形成连通域。
                CvInvoke.Line(Request.GrayToDst, new Point(0, 0), Request.WaterRulerTopPoint, new MCvScalar(255, 255, 255), 2);

                FindContoursENextProc(Request);
            }
            else
            {
                if (null != NextProcesser)
                    NextProcesser.ProcessRequest(Request);
            }
        }

        public override string ToString()
        {
            return "Proc_6_OtsuErode"; //|EqualizeHist|Threshold|Erode|Dilate
        }
    }
}
